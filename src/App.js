// import logo from './logo.svg';
import './App.css';
import './assets/styles/styles.scss'
import 'bootstrap/dist/css/bootstrap.min.css';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'

import stores from './redux/stores';
import AppRoute from './routes/route';

let persistor = persistStore(stores);

function App() {
  return (
    <Provider store={stores}>
      <PersistGate loading={null} persistor={persistor}>
        <AppRoute />
      </PersistGate>
    </Provider>
  );
}

export default App;
