import * as actionType from '../actionTypes'
import { all, call, put, takeLatest } from 'redux-saga/effects';
import * as config from '../../config/config'

export function* getUsers(param) {
    try {
        let _response = yield call(config.GET, config.URL.user + '/' + param.value)
        yield put({ type: actionType.GET_DATA_USERS_SUCCESS, value: _response.data })
        localStorage.setItem('auth', true)
        param.history.push(`/repository/${param.value}`)

    } catch (error) {
        console.log('error : ', error)
    }
}

export function* getRepo(param) {
    try {
        let _response = yield call(config.GET, config.URL.user + '/' + param.value + '/repos')
        yield put({ type: actionType.GET_DATA_REPO_SUCCESS, value: _response.data })

    } catch (error) {
        console.log('error : ', error)
    }
}

export default function* rootSaga() {
    yield all([
        takeLatest(actionType.GET_DATA_USERS, getUsers),
        takeLatest(actionType.GET_DATA_REPO, getRepo),
    ])
}