import {
    HANDLE_STATE, GET_DATA_USERS_SUCCESS, GET_DATA_REPO_SUCCESS
} from '../actionTypes'

const initState = {
    username: '',
    isShow: false,
    data: {},
    repository: []
}

const repositoryReducer = (state = initState, action) => {
    switch (action.type) {
        case HANDLE_STATE:
            return {
                ...state, [action.field]: action.value
            }
        case GET_DATA_USERS_SUCCESS:
            return {
                ...state, data: action.value
            }
        case GET_DATA_REPO_SUCCESS:
            return {
                ...state, repository: action.value
            }
        default:
            return state
    }
}

export default repositoryReducer