import * as actionType from '../actionTypes'

export const handleState = (field, value) => {
    return {
        type: actionType.HANDLE_STATE,
        field,
        value
    }
}

export const searchUsername = () => {
    return {
        type: actionType.ON_CLICK_USERNAME
    }
}

export const getDetailData = (value, history) => {
    return {
        type: actionType.GET_DATA_USERS,
        value,
        history
    }
}
export const getListRepo = (value) => {
    return {
        type: actionType.GET_DATA_REPO,
        value
    }
}