import { all } from 'redux-saga/effects';

import RepositorySagas from "../redux/reducer/sagas"

export default function* rootSaga(getState) {
    yield all([
        RepositorySagas()
    ]);
}