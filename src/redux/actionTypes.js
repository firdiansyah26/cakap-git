export const GET_DATA_USERS = 'GET_DATA_USERS'
export const GET_DATA_USERS_SUCCESS = 'GET_DATA_USERS_SUCCESS'
export const GET_DATA_REPO = 'GET_DATA_REPO'
export const GET_DATA_REPO_SUCCESS = 'GET_DATA_REPO_SUCCESS'
export const HANDLE_STATE = 'HANDLE_STATE'
export const ON_CLICK_USERNAME = 'ON_CLICK_USERNAME'