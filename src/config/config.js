import axios from 'axios';

const BASE_URL = 'https://api.github.com'
const HEADER = {
    'Content-Type': 'application/json'
}

export const URL = {
    user: '/users'
}

// GLOBAL API METHOE
export function GET(url) {
    return axios.get(BASE_URL + url, HEADER)
        .then((res) => {
            return res
        })
        .catch((err) => {
            return err
        })
}

export function PATCH(url, body) {
    return axios.patch(BASE_URL + url, body, HEADER)
        .then((res) => {
            return res
        })
        .catch((err) => {
            return err
        })
}
