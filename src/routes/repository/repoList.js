import React, { useCallback } from 'react'
import { Collapse, Row, Col, Button, Container, Dropdown, DropdownButton } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { handleState } from "../../redux/reducer/actions";
import { StarOutlined, EyeOutlined, ForkOutlined } from '@ant-design/icons'
import '../../assets/styles/styles.scss'

const RepoList = () => {
    let state = useSelector(state => state.RepositoryReducer);
    let dispatch = useDispatch();

    let onClickOpen = useCallback(() => {
        dispatch(handleState('isShow', !state.isShow))
    }, [dispatch])

    return (
        <React.Fragment>
            <Collapse in={state.isShow}>
                <Row>
                    <Col className="repository-page">
                        <div className="div-position">
                            <Button variant="secondary" size="sm" onClick={() => onClickOpen()}> Profile</Button>
                        </div>
                        <h4 className="text-center">My Repositories</h4>
                        <Row>
                            <Col>
                                {
                                    state.repository.map((obj, idx) => {
                                        return (
                                            <div key={idx}>
                                                <Row className="repository-list row-margin" style={{ margin: '2% 0%' }}>
                                                    <Container>
                                                        <Row className="row-underline">
                                                            <Col sm={8}><b>{obj.name}</b></Col>
                                                            <Col sm={1}><StarOutlined /> {obj.stargazers_count}</Col>
                                                            <Col sm={1}><EyeOutlined /> {obj.watchers_count}</Col>
                                                            <Col sm={1}><ForkOutlined /> {obj.forks_count}</Col>
                                                        </Row>
                                                        <Row>
                                                            <Col sm={8}>Language : {obj.language}</Col>
                                                            <Col sm={4}>
                                                                <DropdownButton style={{ position: 'absolute' }} id="" size="sm" variant="secondary" title="Select">
                                                                    <Dropdown.Item href="#">Clone or Download</Dropdown.Item>
                                                                </DropdownButton>
                                                            </Col>
                                                        </Row>
                                                        <Row><Col sm={8}>Size : {(obj.size / (1024)).toFixed(2)} MB</Col></Row>
                                                        <Row><Col sm={8}>Forking URL : {obj.forks_url}</Col></Row>
                                                        <Row><Col sm={8}>Description : {obj.description}</Col></Row>
                                                    </Container>
                                                </Row>
                                            </div>
                                        )
                                    })
                                }
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Collapse>
        </React.Fragment>
    )
}

export default RepoList