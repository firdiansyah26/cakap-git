import React, { useCallback, useEffect } from 'react'
import { Container, Row, Col, Image, Button, Figure } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import RepoList from './repoList'
import { Link } from 'react-router-dom';
import { handleState, getListRepo } from "../../redux/reducer/actions";
import '../../assets/styles/styles.scss'

const Repository = () => {
    let state = useSelector(state => state.RepositoryReducer);
    let dispatch = useDispatch();

    let onClickOpen = useCallback(() => {
        dispatch(handleState('isShow', !state.isShow))
    }, [dispatch])

    let onClickBack = useCallback(() => {
        dispatch(handleState('data', {}))
        dispatch(handleState('repository', []))
        localStorage.removeItem('auth')
    })

    useEffect(() => {
        dispatch(getListRepo(state.data.login))
    }, [])

    return (
        <React.Fragment>
            <Container>
                {
                    !state.isShow ?
                        <Row>
                            <Col className="repository-page">
                                <h4 className="text-center">Github Profile</h4>
                                <Row>
                                    <Col xs={3}>
                                        <Row>
                                            <Image style={{ margin: 'auto' }} src={state.data.avatar_url} roundedCircle width="181" height="181" />
                                        </Row>
                                        <Row className="row-margin">
                                            <Button className="repository-center button-padding" variant="secondary" size="sm" onClick={() => onClickOpen()}>My Repository</Button>
                                        </Row>
                                        <Row className="row-margin">
                                            <Link className="repository-center button-padding" to="/" onClick={() => onClickBack()}><Button style={{ width: '19.5vh' }} variant="secondary" size="sm">Back</Button></Link>
                                        </Row>
                                    </Col>
                                    <Col className="col-desc">
                                        <Row>
                                            <h5>{state.data.name}</h5>
                                        </Row>
                                        {
                                            state.data.login ?
                                                <Row>
                                                    <h5 className="text-id">@{state.data.login}</h5>
                                                </Row> : ''
                                        }
                                        <Row>
                                            <Figure>
                                                <Figure.Caption>{state.data.bio}</Figure.Caption>
                                            </Figure>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        : null
                }
                {
                    state.isShow ? <RepoList /> : null
                }
            </Container>
        </React.Fragment >
    )
}

export default Repository