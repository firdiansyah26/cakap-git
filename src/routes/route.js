import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Login from './login/login'
import Repository from './repository/repository'


const AppRoute = () => {
    return (
        <Router basename="/">
            <Switch>
                {/* <Route path="/" exact>
                    <Home />
                </Route> */}
                <Route path="/repository/:idUser">
                    <Repository />
                </Route>
                <Route path="/" exact>
                    <Login />
                </Route>
            </Switch>
        </Router>
    );
};


export default AppRoute;
