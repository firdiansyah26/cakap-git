import React, { useCallback, useState } from 'react'
import { InputGroup, FormControl, Button } from 'react-bootstrap';
import { handleState, getDetailData } from '../../redux/reducer/actions'
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";

import '../../assets/styles/styles.scss'

const Login = () => {
    let state = useSelector(state => state.RepositoryReducer);
    let dispatch = useDispatch();
    let history = useHistory();

    let [isShowAlert, setIsShowAlert] = useState(false)

    let onChangeState = useCallback((field, value) => {
        dispatch(handleState(field, value))
        if (value) setIsShowAlert(false)
    }, [dispatch])

    let onClickSearch = useCallback(() => {
        if (state.username) dispatch(getDetailData(state.username, history))
        else setIsShowAlert(true)
    }, [state, dispatch])

    return (
        <React.Fragment>
            <div className="background-home ">
                <div className="login-form">
                    <h6 className="text-left">Input your github Username</h6>
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="Github Username"
                            aria-label="Github Username"
                            aria-describedby="basic-addon2"
                            onChange={(e) => onChangeState('username', e.target.value)}
                            value={state.username}
                        />
                        <InputGroup.Append>
                            <Button onClick={() => onClickSearch()} variant="outline-secondary">Submit</Button>
                        </InputGroup.Append>
                    </InputGroup>
                    {
                        isShowAlert ? <text className="text-validation">You must fill the username!</text> : null
                    }
                </div>
            </div>
        </React.Fragment>
    )
}

export default Login